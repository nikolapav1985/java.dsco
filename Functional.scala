/**
* object Functional
*
* functional approach used to compute a sum of integers
* in this case, only specify input and conditions
* output should depend on input alone (no local state)
*
* ----- compile -----
*
* scalac Functional.scala
*
* ----- run example -----
*
* scala Functional
*
* ----- example output -----
*
* 1
* 2
* 3
* 4
* 10
*
*/
object Functional extends App {
    val list=List.range(1,5)
    list.foreach(println)
    println(sum(list))
    def sum(ints: List[Int]): Int = ints match {
        case Nil => 0
        case x::tail => x+sum(tail)
    }
}
