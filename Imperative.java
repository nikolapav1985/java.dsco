import java.util.*;
import java.lang.*;
/**
* class Imperative
*
* used to compute sum of integers using imperative approach
* that is, specify exact steps (including state and local variables) to reach output
*
* ----- compile -----
*
* javac Imperative.java
*
* ----- run example -----
*
* java Imperative 5 11
*
* ----- example output -----
*
* Array content
* 9 6 6 2 4  
* 27
*
*/
class Imperative{
    public static void main(String[] args){
        int count=0;
        int max=0;
        int i=0;
        int sumdone=0;
        int arr[];
        Random r;
        if(args.length != 2){ // need array length and max item as command line arguments
            System.exit(1);
        }
        count=Integer.parseInt(args[0]);
        max=Integer.parseInt(args[1]);
        arr=new int[count];
        r=new Random();
        for(i=0;i<count;i++){
            arr[i]=r.nextInt(max);
        }
        System.out.println("Array content");
        for(i=0;i<count;i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println(" ");
        sumdone=sum(arr);
        System.out.println(sumdone);
    }
    public static int sum(int[] arr){
        int i;
        int sum=0; // local state to track sum
        for(i=0;i<arr.length;i++){ // compute sum
            sum=sum+arr[i];
        }
        return sum;
    }
}
