Discussion 1
------------

- Imperative.java (imperative example, check comments for details)
- Functional.scala (functional example, check comments for details)

Test environment
----------------

- os lubuntu 16.04 kernel version 4.13.0
- javac version 11.0.5
- scalac version 2.13.2
